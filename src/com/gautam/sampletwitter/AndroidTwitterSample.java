package com.gautam.sampletwitter;

import java.util.ArrayList;
import java.util.List;

import oauth.signpost.OAuth;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.http.AccessToken;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AndroidTwitterSample extends Activity {

	private SharedPreferences prefs;
	private final Handler mTwitterHandler = new Handler();
	private EditText tweetMessage;
	Button ShowStatus;
	Button Timeline;
	Button mentions;
	Button directMessage;
	Button getHashTag;
	Button search;

	final Runnable mUpdateTwitterNotification = new Runnable() {
		public void run() {
			Toast.makeText(getBaseContext(), "Tweet sent !", Toast.LENGTH_LONG)
					.show();
			tweetMessage.setText("");
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
		tweetMessage = (EditText) findViewById(R.id.editText1);
		Button tweet = (Button) findViewById(R.id.btn_tweet);
		Button clearCredentials = (Button) findViewById(R.id.btn_clear_credentials);
		ShowStatus = (Button) findViewById(R.id.button1);
		Timeline = (Button) findViewById(R.id.button2);
		mentions = (Button) findViewById(R.id.button3);
		directMessage = (Button) findViewById(R.id.button4);
		getHashTag = (Button) findViewById(R.id.button5);
		search = (Button) findViewById(R.id.button6);

		search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				List<String> lsit = new ArrayList<String>();
				Twitter twitter = new TwitterFactory().getInstance();
				String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
				String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

				AccessToken a = new AccessToken(token, secret);
				twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
						Constants.CONSUMER_SECRET);
				twitter.setOAuthAccessToken(a);
				Query query = new Query(tweetMessage.getText().toString());
				QueryResult result;
				try {
					result = twitter.search(query);
					for (Tweet tweet : result.getTweets()) {
						lsit.add(tweet.getFromUser() + ":" + tweet.getText());
					}
					Search.a11 = lsit;
					Intent intent = new Intent(AndroidTwitterSample.this,
							Search.class);
					startActivity(intent);
				} catch (TwitterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		getHashTag.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
					String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET,
							"");

					AccessToken a = new AccessToken(token, secret);
					Twitter twitter = new TwitterFactory().getInstance();
					twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
							Constants.CONSUMER_SECRET);
					twitter.setOAuthAccessToken(a);
					Log.v("USERID", twitter.getId() + "");
					List<Status> a1 = twitter.getFavorites();
					Statuses.a11 = a1;
					Intent intent = new Intent(AndroidTwitterSample.this,
							Statuses.class);

					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		directMessage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
					String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET,
							"");
					AccessToken a = new AccessToken(token, secret);
					Twitter twitter = new TwitterFactory().getInstance();
					twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
							Constants.CONSUMER_SECRET);
					twitter.setOAuthAccessToken(a);
					Log.v("USERID", twitter.getId() + "");
					List<Status> a1 = twitter.getPublicTimeline();
					Statuses.a11 = a1;
					Intent intent = new Intent(AndroidTwitterSample.this,
							Statuses.class);
					startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		mentions.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
					String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET,
							"");

					AccessToken a = new AccessToken(token, secret);
					Twitter twitter = new TwitterFactory().getInstance();
					twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
							Constants.CONSUMER_SECRET);
					twitter.setOAuthAccessToken(a);
					Log.v("USERID", twitter.getId() + "");

					List<Status> a1 = twitter.getMentions();
					Statuses.a11 = a1;
					Intent intent = new Intent(AndroidTwitterSample.this,
							Statuses.class);

					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		Timeline.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
					String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET,
							"");

					AccessToken a = new AccessToken(token, secret);
					Twitter twitter = new TwitterFactory().getInstance();
					twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
							Constants.CONSUMER_SECRET);
					twitter.setOAuthAccessToken(a);
					Log.v("USERID", twitter.getId() + "");

					List<Status> a1 = twitter.getHomeTimeline();
					Statuses.a11 = a1;
					Intent intent = new Intent(AndroidTwitterSample.this,
							Statuses.class);

					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		ShowStatus.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
					String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET,
							"");

					AccessToken a = new AccessToken(token, secret);
					Twitter twitter = new TwitterFactory().getInstance();
					twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
							Constants.CONSUMER_SECRET);
					twitter.setOAuthAccessToken(a);
					Log.v("USERID", twitter.getId() + "");

					List<Status> a1 = twitter.getUserTimeline();
					Statuses.a11 = a1;
					Intent intent = new Intent(AndroidTwitterSample.this,
							Statuses.class);

					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		tweet.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (TwitterUtils.isAuthenticated(prefs)) {
					sendTweet();
				} else {
					Intent i = new Intent(getApplicationContext(),
							PrepareRequestTokenActivity.class);
					i.putExtra("tweet_msg", getTweetMsg());
					startActivity(i);
				}
			}
		});

		clearCredentials.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				clearCredentials();

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	private String getTweetMsg() {
		return tweetMessage.getText().toString();
	}

	public void sendTweet() {
		Thread t = new Thread() {
			public void run() {

				try {
					TwitterUtils.sendTweet(prefs, getTweetMsg());
					mTwitterHandler.post(mUpdateTwitterNotification);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		};
		t.start();
	}

	private void clearCredentials() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		final Editor edit = prefs.edit();
		edit.remove(OAuth.OAUTH_TOKEN);
		edit.remove(OAuth.OAUTH_TOKEN_SECRET);
		edit.commit();
	}

}