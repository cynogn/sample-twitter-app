package com.gautam.sampletwitter;

import java.util.ArrayList;
import java.util.List;

import oauth.signpost.OAuth;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.http.AccessToken;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Details extends Activity {

	TextView name;
	TextView tag;
	TextView tweet;
	TextView time;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dispaly_details);

		name = (TextView) findViewById(R.id.screenname);
		tag = (TextView) findViewById(R.id.tag);
		tweet = (TextView) findViewById(R.id.tweet);
		time = (TextView) findViewById(R.id.time);

		final Status a = StatucStatic.StatusValue;
		name.setText(a.getUser().getName().toString());
		tag.setText(a.getUser().getScreenName().toString());
		tweet.setText(a.getText().toString());
		time.setText(a.getUser().getCreatedAt().toString());
		name.setOnClickListener(new OnClickListener() {

			private Twitter twitter;

			public void onClick(View v) {

				List<Status> a1;
				try {

					SharedPreferences prefs;
					prefs = PreferenceManager
							.getDefaultSharedPreferences(Details.this);
					Log.v("PREFERENCE", prefs.toString());
					Twitter twitter = new TwitterFactory().getInstance();
					String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
					String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET,
							"");

					AccessToken a123 = new AccessToken(token, secret);
					twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
							Constants.CONSUMER_SECRET);
					twitter.setOAuthAccessToken(a123);
					a1 = twitter.getUserTimeline(a.getUser().getScreenName());

					Statuses.a11 = a1;
					Intent intent = new Intent(Details.this, Statuses.class);
					startActivity(intent);
				} catch (TwitterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}
}
