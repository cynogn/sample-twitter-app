package com.gautam.sampletwitter;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Statuses extends Activity {
	ListView mListViewOfFriends;
	private ArrayList<String> array = new ArrayList<String>();;
	public static List<Status> a11 = new ArrayList<Status>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listoffriends);
		array.clear();
		for (Status status : a11) {
			String a = status.getUser().getName() + ":" + status.getText();
			array.add(a);
		}
		mListViewOfFriends = (ListView) findViewById(R.id.listView);
		mListViewOfFriends.setAdapter(new ArrayAdapter(this, a11));
		mListViewOfFriends.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				StatucStatic.StatusValue = (Status) a11.get(arg2);
				Intent i = new Intent(Statuses.this, Details.class);
				startActivity(i);
			}
		});
	}

	class ArrayAdapter extends BaseAdapter {
		List<Status> a11;
		Context context;

		ArrayAdapter(Context con, List<Status> a11) {
			this.context = con;
			this.a11 = a11;
		}

		public int getCount() {
			return a11.size();
		}

		public Object getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.rowlist, parent, false);
			TextView textView = (TextView) rowView.findViewById(R.id.label);
			textView.setText(array.get(position) + "");
			return rowView;
		}
	}
}
